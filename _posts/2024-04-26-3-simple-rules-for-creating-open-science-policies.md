---
layout: single
title:  "3 simple rules for creating Open Science Policies"
date:   2024-04-26
excerpt: "What are good strategies to develop an Open Science Policy? Here's our opinionated guide. This post was co-created with Sander Bosch, Open Science Programme Coordinator at VU Amsterdam."
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/pcq1eA2oAqGnx9q3vwW45v
---


At the [Open Science Retreat 2024](https://openscienceretreat.eu/) Sander and I had a conversation about his experience in policy development. Since we believe that his experience is valuable for a lot of people out there, we decided to write down three simple rues to create an Open Science policy:
1. Develop output policies first and combine them into an Open Science policy later.
2. Policies are about the "what" and "why". The "how" belongs elsewhere (e.g. guidelines).
3. Don't start with the policy, start with the guidelines.

## Rule 1: Develop output policies first and combine them into an Open Science policy later.
Let's say you are in charge of creating an Open Science policy for your institution. If you create an Open Science policy from the get go, this will usually lead to a minimal consensus. Too many people need to be involved and forward thinking ideas may be shut down. Instead do the following:

- **Develop output policies first:** a policy for open source, a policy for FAIR/open data, a policy for open access, ...
- **Develop the Open Science policy second:** it's really easy, contains a statement at the top ("We value open scholarship and apply best research practices...") and then essentially just links to the output policies.

![main lego block: open science, pieces: open source, data, open access, ...]({{ site.url }}/assets/images/posts/2024-04-26_os-policy.jpeg)

## Rule 2: Policies are about the "what" and "why". The "how" belongs elsewhere.
Policies are about what to do and why to do it. The implementation steps (how to it), belong in guidelines or similar support documents. 

For example:
- **Policy**: We want all publications from members of our organization to be openly accessible (WHAT), because we believe that open access promotes equity and increases effectiveness of scientific progress (WHY).
- **Guideline**: To ensure that your publication is open access, you can publish an open access preprint version, publish in an open access journal, or discuss alternatives with the open access support team (HOW).

The "what" and "why" will stay relatively constant over time, but the how may change rather quickly depending on technological advances, changes in publication venues, or the organization's support structure.

## Rule 3: Don't start with the policy, start with the guidelines.
In order to know what a policy should encompass, it is helpful to understand best practices first. Let's say you are developing an open source policy. It's good to know what good strategies for open source development are and what the organization's members already do before you get to the "what" and "why" of the policy. If you do this, you can make sure that there’s no huge gap between what the policy demands and what is **feasible, supported and useful in practice** (gaps like these are there surprisingly often!). You can first develop the guidelines (HOW) and then the policy (WHAT and WHY) or develop them simultaneously.

![Option 1: First guidelines, then policy, Option 2: Simlutaneous development]({{ site.url }}/assets/images/posts/2024-04-26_guidelines-policy.jpg)

Example: Open Source Policy development
- Run a workshop with people who write code and support with code at your organization. Ask them about best practices and also what the reasons for applying these practices are to them. 
- Depending on what you learn, you might decide to jointly develop a guideline for best practices on (1) open source software development and licensing and (2) FAIR code scripts. 
- During these discussions you might notice values coming up over and over again: quality (over quantity), reusability, equity, or simply openness. Take these and use them for the policy. 

We hope these 3 rules help you create policies for your organization. Please forward this information to anyone who might benefit.

This post and all images are licensed under CC-BY 4.0. Feel free to reuse them. Please link back to this post. 

All the best,   
Sander and Heidi
