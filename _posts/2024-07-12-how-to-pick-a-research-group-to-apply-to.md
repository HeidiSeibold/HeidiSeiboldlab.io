---
layout: single
title:  "How to pick a research group to apply to?"
date:   2024-07-12
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/2e6knm6FZeZ2Hb7MVaGhSZ
excerpt: "I get asked for career advice all the time (even though I am just figuring stuff out myself). Generally I try to help by listening and asking questions, but there is one thing that I tell everyone who wants to hear it: pick work where you like the people."
---







How do you pick the research group you want to work with? My recommendation is to pick based on two things:
1. Do you like the topics they work on?
2. Do you get along with the people in the group (in particular your boss/supervisor)?

The first is quite obvious for most people, but the fact that the second may be even more important is not as clear. 

Team dynamics matter so much! I've seen people start a (PhD) position and be super unhappy, because their supervision and/or team work sucked. 

Sometimes it is difficult to know before, but there are a few things one can do:
- Talk to the people who work in the group, collaborate with the group, or did so in the past: Do they like working with the people? What are group meetings like? How does supervision work? How is collaboration managed? Are there team events?
- Ask questions to the future boss (e.g. in the job interview): How do they view supervision? How do they manage the group? How is collaboration supported?

![3 hikers in the dusk](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/2e6knm6FZeZ2Hb7MVaGhSZ){: width="450" style="display: block; margin: 0 auto" }

Doing research is tough and you need to be able to depend on each other in a team. Just like when you go on a challenging multi-day hike: you bring people you can trust and who you will be able to stand for that amount of time. 

**This hike is your work and it will last months or years.**  

If the group dynamics are toxic you can't do good work, you'll suffer, and all you can really do in most cases is leave. Because at least in Germany, academic institutions are not built to throw out toxic leaders. So if you can, spare yourself the trouble and pick nice people to work with.

I wish it were a simple as it sounds, but I know it isn't. Sending out a big hug to everyone who currently is in a situation where they don't like working with the people around them 💜.

All the best,   
Heidi  
