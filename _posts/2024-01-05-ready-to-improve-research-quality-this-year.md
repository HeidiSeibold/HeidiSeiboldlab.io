---
layout: single
created: 2024-08-02T09:20:16 (UTC +02:00)
title: "Ready to improve research quality this year?"
date: 2024-01-05
excerpt: "A new year always comes with the feeling of a new beginning. Let's look at what's to come this year. What are you up to? Do you have any resolutions? Ready to take the next step towards more open and collaborative research?"
---


On new year's eve, my husband and I have a habit of looking back on the year. It's incredible how my life changed in 2023 compared to the years before, especially my professional life. Yet my professional drivers have stayed the same: **improve the quality of research**.

I am happy to report that I feel like I had more impact on the quality of research in 2023 than any other year.

!["Happy new year!" letters next to a researcher in a labb](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/a8QoBWk2ZoY5VKvYn1iYgc)

## 2024

Let's look forward, to 2024: What can we do to go even further? Both you and I. How can we improve the quality of research in 2024? Here are some ideas.

<table><tbody><tr><td as="td"><div><h2>You</h2><ol><li><span>Learn about ways to make your research better.</span></li><li><span>Help other people improve their work.</span></li></ol></div></td><td></td><td as="td"><div><h2>Heidi</h2><ol><li><span>Offer possibilities to learn about good research practices.</span></li><li><span>Give others who do good work visibility and possibilities to share.</span></li></ol></div></td></tr></tbody></table>

## You

### Learn about ways to make your research better.

There is so many ways you can make your research better. There is always something to learn. For example, you could...

-   take a course on open and reproducible research (reach out to me if you are interested 😉),
-   participate in the [Open Science Challenge](https://heidiseibold.ck.page/opensciencechallenge) and learn more about your own goals,
-   check out [The Turing Way](https://the-turing-way.netlify.app/index.html), a free online book on reproducible, ethical and collaborative data science,
-   join the [Open Science Retreat](https://openscienceretreat.eu/apply/) and discuss with like-minded people,
-   join/organize a [ReproducibiliTea](https://reproducibilitea.org/) journal club or a code club,
-   and many more...

### Help other people improve their work

You probably already know a thing or two about good research practices.

-   Are you the nerd who knows how to improve coding practices? Help someone by going through their code together.
-   Are you an open access hero? Share your story with others.
-   Want to share your knowledge more broadly? Consider teaching (e.g. by [joining the Digital Research Academy as a trainer](https://digital-research.academy/trainers-join/)).

## Heidi

### Offer possibilities to learn about good research practices.

This year I want to support people in research in the fields of open science, reproducible research, research software engineering and data literacy. How? Various ways:

-   Organize workshops and trainings for PhD programs, research groups, and other interested parties.
-   Send out ideas and advice through my newsletter.
-   Bring these topics to events (see e.g. our workshop on [FAIR and reproducible code at deRSE24](https://events.hifis.net/event/994/contributions/7987/), organized with the German Reproducibility Network)
-   Offer easily accessible online courses through the Digital Research Academy [basic membership](https://digital-research.academy/learners/#membership).
-   ...



### Give others who do good work visibility and possibilities to share.

Through the [Digital Research Academy](https://digital-research.academy/) we have created the possibility for experts to share their expertise and become part of a trainer network. This year I want to spend a good amount of my energy to giving these wonderful people the visibility and possibilities they deserve.

Through our website and social media accounts you'll soon be able to learn more about the types of trainings the DRA trainers offer.

[Mastodon](https://mastodon.social/@digiresacademy/) | [LinkedIn](https://www.linkedin.com/company/digital-research-academy/) | [Website](https://www.linkedin.com/company/digital-research-academy/)

An we'll onboard even more trainers this year. Our next Train-the-Trainer program starts end of January and we're already planning another for summer. Interested in becoming a trainer? Sign up for [this mailing list](https://www.listserv.dfn.de/sympa/subscribe/dra) to not miss any upcoming opportunities!

Each training will be bespoke to fit the needs of the respective learners. I am sure this will be a fun endeavor for learners, trainers, as well as us in the core team.

## Your input needed

Before I close this post, let me ask you for your help.

In one of the upcoming newsletter posts, I want to write about code clubs and hacky hours. Do you have any experience, know of any (besides the ones listed [here](https://fosstodon.org/@HeidiSeibold/111261036485336617)), or know of good resources on how to organize them? Please message me or respond to [this toot](https://fosstodon.org/@HeidiSeibold/111261036485336617) on Mastodon. Thank you!

All the best,

Heidi

