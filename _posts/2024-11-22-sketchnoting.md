---
layout: single
title:  "Sketchnoting"
date:   2024-11-22
excerpt: "Taking notes and drawing little images helps me think, but they are also helpful to others. Would sketchnoting also be for you?"
header:
  teaser: /assets/images/posts/2024-11-22_sketchnotes-course.jpg
---

I started sketchnoting by taking notes about scientific presentations and meetings. 
They help me think and keep me focused on what is said. 
If used to find it very hard to concentrate during presentations and I found a couple of ways to focus on what is said and trying to really understand it.
One of them is making myself come up with questions about the talk (a skill that now also helps me be a good moderator) and the other is sketchnoting. 

For example, I took notes of the CERN/NASA Open Science Summit last year. 
You can find an example below and the rest of the sketchnote summaries on the [event's Zenodo page](https://zenodo.org/communities/cern-nasa-open-science-summit-july-2023) (search for "sketch notes").

![](/assets/images/posts/2024-11-22_cern-sketchnotes.jpg){: width="600" .align-center}

I truly think that sketchnoting is a skill more people should foster and - and I know many of you will doubt this - anyone can learn it.
It is not only helpful for staying focused, it also helps communicate better.
As people working in research, communicating complex topics is such an important skill!

### Why do I think anyone can learn to sketchnote?
Anyone can write with pen an paper can learn to sketchnote, because if you know how to draw really simple things - like a circle and an arrow - and you also know how to take notes, you're halfway there. And all you need as tools is one pen and one piece of paper.

![](/assets/images/posts/2024-11-22_simple-sketchnotes.jpg){: width="700" .align-center}

**It does not have to be super pretty all the time.** 
For example, Chris Lysy who actually makes a living with his simple cartoons and sketchnotes doesn't actually have a handwriting most people would admire.
But that's what makes his work so approachable. See, for example his blog post on [amplifying conference presentations](https://freshspectrum.com/amplify-conference/).

### Why not start today?
Since people keep asking me how I learned sketchnoting, I decided to create a sketchnoting email course.
It's free, you will receive 9 emails (one email every workday for 9 days) and at the end you will have your first sketchnotes ready.

![](/assets/images/posts/2024-11-22_sketchnotes-course.jpg){: width="700" .align-center}

Sounds good? Give it a try and sign up below! 
<p align="center">
<script async data-uid="2cab44558f" src="https://heidiseibold.kit.com/2cab44558f/index.js"></script>
</p>
Note that the course starts once you sign up. If you want to start later, sign up at a later point in time.


