---
layout: single
title:  "Data literacy in the public sector"
date:   2024-06-07
excerpt: "Have you complained about the inefficiency of public administration before? I think, you're not alone. In this post I want to share my journey with trying to help increase efficiency through data literacy in the public sector."
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/t57jKaoLjVNuRK6ExEbqgQ
---

I am a person who likes to solve problems. Most of my time is spent solving problems in academia, but I decided to leave my usual grounds for a special project where I can help solving problems in the public sector.

Why do I care about solving problems in the public sector? Public service is for us, the public. If it's inefficient, we are all harmed. **Some inefficiencies can be solved by increasing data literacy in public servants.**

Some examples where data literacy is not only useful, but required are
- evidence-based policy making, 
- monitoring the health of our forests, 
- creating random samples of court cases for archival, or
- improved automation and digitization in the municipal office.

A risk that the public sector faces if nobody with data literacy is involved in decision making on these topics is that **service providers can sell services and products that are unachievable, overpriced, or simply not useful** for the public servants who are supposed to benefit and make use of digital tools or knowledge.

## Boosting digitization of public administration
In our project [ADA Bayern](https://ada-oeffentliche-verwaltung.de/) (Applied Data Analysis for the public sector in Bavaria), we run workshops with public servants, IT specialists, and data scientists to set up or improve the use of data. This data is mostly already available, but underutilized, because the mindset and skills for using it are lacking. We're here to help out with that. 

We, that is a wonderful [team](https://ada-oeffentliche-verwaltung.de/team.html) of researchers from LMU Munich ([SODA lab](https://www.stat.lmu.de/soda/en/)) and me in collaboration with the [Bavarian State Ministry for Digital Affairs](https://www.stmd.bayern.de/).

- **We work on solving specific problems.** For example: How to efficiently and reliably decide which court files should be archived long term?
- **People work in teams to create a product which solves the problem.** For example: A data scientist, a IT expert, a jurist, and an archivist create an app that creates a random sample of the court cases using the metadata of court cases which is available as a CSV file.
- **Everyone brings their skills to the table and learns new skills from others.** For example: Data scientists learn from archivists on how archival of court cases currently works. Archivists and Jurists learn about creating graphs and doing data analysis with the metadata of court cases. 
- **We provide infrastructure, to run the course in a secure cloud environment.** For example: We set up a cloud environment that participants can access through the browser and looks like a desktop. The data, R, RStudio and other needed building blocks are readily available after the secure login and an initial data safety training.

[![Solve problems, Team Work, Learn from each other, Cloud](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/t57jKaoLjVNuRK6ExEbqgQ){: width="650" style="display: block; margin: 0 auto" }](https://ada-oeffentliche-verwaltung.de/)

We believe that through this setup we can help not only equipping public servants with basic data literacy skills, but we help them overcome their fear of data and data-related tools, equip them with the ability to make informed decisions about service providers, and together with them solve a real problem they have.

In our first workshop series with the state archives and the courts we were able to make tremendous progress and the archivists were more than happy about the solution to their problem. 



## Next steps
We are now looking for other problems we can help to solve. Optimally in Bavaria, as we have funding from the Bavarian State Ministry for Digital Affairs to offer workshops to the public sector in Bavaria for free.
Know anyone who could benefit from our support? 
[Get in touch!](data-analytics@stat.uni-muenchen.de)

Looking forward to your ideas!

All the best,   
Heidi
