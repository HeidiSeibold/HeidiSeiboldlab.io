---
layout: single
title:  "A pragmatic Open Scientists Guide to Publishing Papers "
date:   2024-06-28
excerpt: "The academic publishing system is broken. I think we can all agree on that. But what if you want to have an academic career and at the same time stick to your values of openness? Here's my pragmatic take."
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/9xk8RzEnf8R3RkfT97HCcL
---


I understand the fear of not publishing in established journals. We all want to have a good career and feel like publishing our papers in the journals that our peers and employers deem worthy seems like an important step. As a pragmatic open scientist, I generally recommend not to be too hard on yourself and start your Open Science journey with the steps that feel easy to implement. So here are some options for your journey towards open access:
- Publish a preprint
- Publish in an "established" open access journal
- Publish in a journal that has an open access agreement with your institution/country

![<3 Open Access](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/9xk8RzEnf8R3RkfT97HCcL){: width="450" style="display: block; margin: 0 auto" }


## Option: Publish a preprint
Almost all journals allow you to publish a preprint. Before or at the same time of sending your paper to a journal, you can upload it to a preprint server such as arXiv or OSF preprints. I find ASAPbio's searchable list of preprint servers helpful, as you can also search by field.

To check, if preprints are allowed by the journals you would want to submit to, you can use Sherpa Romeo. For example, let's say you want to publish in Trends in Cognitive Sciences, Sherpa Romeo tells you, you can publish a preprint (see details here).

So the steps to go are:
1. Determine which journal you want to submit to.
2. Check, if they allow preprints (if not, go to another journal and start with 1 😉)
3. Make paper submission-ready.
4. Submit to preprint server and the journal.



**Previous post: Peer Community In**
In a previous post I wrote about Peer Community In (PCI), which uses preprints in a way that circumvents the established publishing system altogether. Sounds interesting?
[Read here](https://heidiseibold.ck.page/posts/peer-community-in-registered-reports){: .btn .btn--primary}
{: .notice}

## Option: Publish in an "established" open access journal
Established open access journals already exist in many fields. If they do exist in your field, then publishing there is a great option. You can browse open access journals by field in the directory of open access journals (DOAJ).   
Publication in these journals can cost money (so-called article processing charges APCs). I find APCs of >4000 Euros unbelievable and would advise against supporting that. Many institutions or funders provide support for APCs. Pro tip: Ask your library for help. They usually know who will pay for you.  
Some journals are free for both authors and readers, like the Journal of Statistical Software, for example.

## Option: Publish in a journal that has an open access agreement with your institution/country
More and more countries, regions, and institutions have agreements with publishers. They pay the publishers money and the authors can publish and read. Many Open Access advocates have mixed feelings about these agreements (see e.g. here or here [DE] on criticism of the German DEAL), but for the individual researcher it can still be a good option.

Examples:
- [German DEAL](https://deal-konsortium.de/en/agreements)
- [Agreements of TU Munich](https://www.ub.tum.de/en/open-access-agreements)

Pro tip: Ask your library for support in finding out more. 

## Not an option: Pay an open access fee in a hybrid journal
Hybrid journals are closed access journals you can pay to publish your paper open access. Librarians have discouraged me from paying for open access in a hybrid journal, as it just means that we pay them even more (tax) money. PlanS has a good article on why hybrid journal are not part of the solution.

If you really want to publish in a hybrid journal, I think it's better not to pay the open access fee, but publish a preprint instead.

## A note on predatory journals
It is a common belief that most open access journals are predatory journals, that just want to make money but don't care about scientific integrity. There are easy steps to recognize predatory journals. See e.g. the Think. Check. Submit. checklist. And most importantly: The belief that "all open access journals are predatory" is a myth!
There are very good open access journals out there!

---

I hope this was a helpful and complete enough guide to pragmatic open access publishing.

All the best,   
Heidi
