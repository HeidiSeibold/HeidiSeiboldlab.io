---
layout: single
title:  "Markdown-based course websites"
date:   2024-12-13
excerpt: "New frameworks for course websites are popping up all the time. In this post, I'll review some of them."
header:
  teaser: /assets/images/posts/2024-12-06_quarto.png
---

Today I want to talk about three interesting frameworks for building course websites: Quarto, the Carpentries Workbench, and Nebula. 

All of them rely on the use of Markdown and are thus easy to version control, reproducible, and reusable. They're all pretty handy when teaching software skills, as they allow incorporating code in a straight-forward way.


| **Tool**              | **Documentation**                                                                                       | **Templates**                                                                                       |
|-----------------------|-------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| **Quarto Websites / Books** | [Website Docs](https://quarto.org/docs/websites/) / [Book Docs](https://quarto.org/docs/books/)       | [Website Template](https://quarto.org/docs/websites/#quick-start) / [Book Template](https://quarto.org/docs/books/#quick-start) |
| **Carpentries Workbench**   | [Docs](https://carpentries.github.io/workbench/)                                                   | [RMarkdown Template](https://github.com/carpentries/workbench-template-rmd), [Markdown Template](https://github.com/carpentries/workbench-template-md) |
| **Nebula**                  | [Docs](https://github.com/esciencecenter-digital-skills/NEBULA-docs/tree/main)                      | [Template](https://github.com/esciencecenter-digital-skills/NEBULA)                                 |

I have created course websites with Quarto before (see e.g. [here]()), I've practiced a little with the Workbench, but Nebula is new to me. 
But let's dive a little deeper into each.

## Quarto websites or books
Both Quarto websites and online books are easy to work with when you've worked with Quarto or RMarkdown before. 
You don't need to [install](https://quarto.org/docs/get-started/) anything but Quarto unless you want to use it in combination with a programming languague or in combination with a tool that is not your pre-installed terminal.

![Quarto book example](/assets/images/posts/2024-12-06_quarto.png)

I like that they provide a straight-forward structure and that a lot of people in my bubble know how to work with Quarto (so I can ask others to contribute, too).
They are pretty enough, but I don't think it's easy to win a design price with Quarto course websites. Yet, they feel modern and are easy to use and mobile-ready out of the box. 
Quarto is flexible enough for my taste and you can even create your own adjusted templates (see Jon Cardoso-Silva's [template for university courses](https://github.com/jonjoncardoso/quarto-template-for-university-courses)).

Example courses:
- [Make Your Research Reproducible](https://berd-nfdi.github.io/BERD-reproducible-research-course/) (Quarto Book)
- [Introduction to Data Science and Statistical Thinking](https://sta199-f24.github.io/) (Quarto Website)


## Carpentries Workbench
The Workbench is first and foremost created for courses of [The Carpentries](https://carpentries.org/), but I've noticed that other initiatives and trainers use it for non-Carpentries courses. It was built using [R](https://r-project.org) and is based on three R packages: sandpaper, pegboard, and varnish. As such you need to install R and the three packages to use it.

![Workbench example](/assets/images/posts/2024-12-06_workbench.png)

The Workbench is perfect for courses in Carpentries style: ‘code with me’-courses where the instructor writes code on their machine and the learners code along. You can switch between learner and instructor view, where for the latter you can incorporate trainer notes.

Example courses:
- [Collaborative Lesson Development Training](https://carpentries.github.io/lesson-development-training/) (with Carpentries design)
- [Real-time analysis and forecasting for outbreak analytics with R](https://epiverse-trace.github.io/tutorials-middle/) (non-Carpentries)


## Nebula
I just learned about Nebula, which is developed by the [eScience Center](https://www.esciencecenter.nl) in the Netherlands.
It uses [Nuxt.js](https://nuxtjs.org/) and [Vue.js](https://vuejs.org/).

![Nebula example](/assets/images/posts/2024-12-06_nebula.png)

The design is so nicely tied in with the eScience Center corporate design, that you don't notice when you switch from their regular website to the training website. The project is a nice example that organisations can create their own Markdown-based flavoured course websites and fast. The project was only started in 2023 and looks quite nice.
Even if it was clearly created for the eScience Center it is available under an Apache license, so you can reuse it if you like the style of these websites.

Example courses:
- [Research Software Support](https://esciencecenter-digital-skills.github.io/research-software-support/)




