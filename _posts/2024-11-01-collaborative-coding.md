---
layout: single
title:  "Collaborative Coding"
date:   2024-11-01
excerpt: "Too many code projects in research are done by a single person. Keep reading on why that's bad and how to do better."
header:
  teaser: /assets/images/posts/collaboration-iceberg.png
---

It's been a while since my last post. Founding the Digital Research Academy, holidays and LOTS AND LOTS of workshops and events have taken all my time. But it's time to start writing again.

One of the workshops I taught since my last post was on collaborative coding at the wonderful [RSE summer school](https://events.hifis.net/event/1467/) at KIT (click through the [timetable](https://events.hifis.net/event/1467/timetable/#all) to find all kinds of cool materials). 

![Collaboration iceberg]({{ site.url }}/assets/images/posts/collaboration-iceberg.png)
Image: There is more to collaboration than we see. The Turing Way project illustration by Scriberia. Used under a CC-BY 4.0 licence. DOI: [10.5281/zenodo.3332807](https://doi.org/10.5281/zenodo.3332807).

*But why is collaboration in coding even relevant? Why teach it at a summer school?*
I think most code projects would benefit from at least two people involved. That includes research software projects, projects that require complex computer simulations, but also research projects with simple data analyses, because

- it is so easy to make mistakes in programming and a second person taking a look just makes sense,
- most of these projects require someone who is more of a coder and someone with more domain expertise, and
- if one person can't continue the project it shouldn't die because of that ([bus factor](https://en.wikipedia.org/wiki/Bus_factor)).

In other words, collaborative coding 

- improves code quality,
- allows for knowledge diversity, and
- promotes longevity. 

Collaboration and with that also collaborative coding is multi-layered and complex. Let's talk about a few of the things that I found helpful for myself when it comes to collaborative coding.

- Setting up a collaborative project
- Collaboration workflows
- Communication and soft skills


## Setting up a collaborative project
To work together effectively, we need to set up our code projects in an organized way, such that everyone in the team (and potential new people to be onboarded) can understand what belongs where and what each file contains. 

The three building blocks that I find most relevant here are:
- good file and folder organisation,
- good naming of files, folder, functions, ..., and
- documentation (at least a README). 

I wrote about setting up a repository in a [previous post](https://heidiseibold.com/2024/03/08/setting-up-a-fair-and-reproducible-project.html). Check that out for awesome templates.


## Collaboration workflows
How do you want to collaborate on a code project? Do you want to do peer coding and sit side by side while you code together? Do you want to split up the work and one person codes the first part, one the second and so on? Do you want to work simultaneously on things?

Whatever you do, I suggest using version control - more specifically Git (with e.g. GitLab/GitHub) - when you work with code. A common collaborative workflow works as follows:

- There is a main branch that is the "stable version" of the code.
- Each collaborator works on a separate branch for their part of the work.
- Once your part is done, you create a Merge Request (that's what it's called on GitLab, "Pull Request" on GitHub), which asks for your branch to be merged with the main branch.
- Someone else from the team reviews the changes and gives feedback. Once everything looks good, they accept the Merge Request.

If your project is popular enought, you may sometimes get contributions from people you don't know. In this case you probably want them to use forks instead of branches.

**Branches:** Branches are essentially separate versions of the codebase within the same repository. They allow developers to work on different features or fixes independently of each other and of the main code.   
**Forks:** A fork is a copy of a repository that exists as a separate project in your GitHub or GitLab account. Forks are commonly used when you don’t have direct access to a repository (e.g., contributing to an open-source project), allowing you to make changes independently before proposing them back to the main repository.
{: .notice--info}

## Communication and soft skills

Communication is key when it comes to good collaboration. The way people in the project behave towards one another can make or break a project. 

As such it makes sense to consider these things. Nowadays many projects have a contributing guide but also a code of conduct. Let me leave you with a list of things to consider when it comes to communication and soft skills and a few helpful links in case you want to dive deeper into it:

- Onboarding and knowledge sharing:
    - [Example CONTRIBUTING.md](https://contributing.md/example/)
- Communication and Code of Conduct:
    - CoC examples: [Scikit learn](https://github.com/scikit-learn/scikit-learn/blob/main/CODE_OF_CONDUCT.md), [FOSS4G:UK](https://uk.osgeo.org/foss4guklocal2023/code-of-conduct), [The Turing Way](https://github.com/the-turing-way/the-turing-way/blob/main/CODE_OF_CONDUCT.md)
    - [Contributor Convenant CoC](https://www.contributor-covenant.org/version/2/1/code_of_conduct/)
- Writing good issues
    - [Tips for good issues](https://dev.to/opensauced/how-to-write-a-good-issue-tips-for-effective-communication-in-open-source-5443)
    - [How to write a bug report](https://code-review.tidyverse.org/issues/)
    - Issue templates on [GitLab](https://docs.gitlab.com/ee/user/project/description_templates.html) and [GitHub](https://docs.github.com/en/communities/using-templates-to-encourage-useful-issues-and-pull-requests/configuring-issue-templates-for-your-repository)
- Giving Credit:
    - [All contributors project](https://allcontributors.org/)
    -  [Software citation guidelines](https://f1000research.com/articles/9-1257)
    - [CRediT taxonomy for publication](https://credit.niso.org/)
- Vision and goals:
    - [Vision and Mission Statements -- a Roadmap of Where You Want to Go and How to Get There](https://www.extension.iastate.edu/agdm/wholefarm/html/c5-09.html)
- Code review:
    - [ROpenScie guide for reviewers](https://devguide.ropensci.org/softwarereview_reviewer.html)
    - [Mozilla review guide](https://mozillascience.github.io/codeReview/review.html)
    
    
You can find all the materials from my workshop on collaborative coding for the RSE summer school on OSF: [DOI:10.17605/OSF.IO/Q76G9](https://doi.org/10.17605/OSF.IO/Q76G9). 
