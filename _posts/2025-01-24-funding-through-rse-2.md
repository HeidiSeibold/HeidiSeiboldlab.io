---
layout: single
title:  "How to Get Funding By Caring About Research Software (II)"
date:   2025-01-24
excerpt: "What are other people doing and what can you do to secure funding for software projects?"
header:
  teaser: /assets/images/posts/git.png
toc: true
toc_sticky: true
---

In a [previous post]({% post_url 2025-01-16-funding-through-rse-1 %}) I wrote about:

> Recent developments on the status of software in research and funding.

Today's post is all about:

> Case studies and practical strategies for securing funding.

Did I miss anything in this post? [Let me know](mailto:heidi@heidiseibold.de) and I will happily add it!
{: .notice--primary}


![](/assets/images/posts/git.png)

# Case Studies

Software can play a very small or a very big part in research projects and thus
in grant applications.
If you develop or want to develop research software, here are a few examples that
may be valuable case studies to learn from.


## Software as essential part of a computational research project 

My PhD project on [Model-Based Recursive Partitioning for Stratified Medicine](https://data.snf.ch/grants/grant/163456) (funded by the Swiss National Science Foundation)
is probably a good example of a project where software was a relevant, but by
far not the only output. My main goal was to develop and adapt statistical/machine 
learning methods for for a specific application: 
stratified or personalized treatment effect estimation.    

When writing the grant, we did not know that we'd create three R packages and 
update another, but software was always planned. Why? Because statistical or
machine learning methods that are not implemented in software are not usable.    

When getting the acceptance of the grant, I remember that the
reviewers liked that the PI always published research software with the methods
and it seemed to have been a clear plus for the grant. 


## Good software leads to further projects



[pySDC](https://parallel-in-time.org/pySDC) is a nice example for a software 
that has received funding from different funders over time, **because** it is
well maintained. 

It was part of [Time-X](https://time-x.eu/), a big EuroHPC project on parallel-in-time integration. 
pySDC was only one of various software outputs including prototypes, proof-of-concept software and application software. 
pySDC has helped the developers to get other funding to continue their work.

[Robert Speck](https://www.fz-juelich.de/profile/speck_r), one of the maintainers of pySDC, 
reports that without their diligent upkeep and extension of the software, 
they would have struggled to become part of the currently running 
BMBF funded-project [StroemungsRaum](https://rmuenste.github.io/scalexa_page/). 


## Software events get funding

I was recently at an event on [digital competencies in science](https://www.volkswagenstiftung.de/en/funding/funding-offer/themed-week-digital-competences-science-completed). 
As co-executive director of the Digital Research Academy, this was of course a very interesting event. 
Even more so as the [workshop that I joined](https://de-rse.org/blog/2024/12/05/vw-symposium.html) was about research software competencies (in German "Forschungssoftwarekompetenzen", one word :D ).
The event was funded by the [Volkswagen Foundation](https://www.volkswagenstiftung.de/en/foundation/about-us), which keeps having interesting funding around software and cares about Open Science.


## Funding for RSE training
At the [Digital Research Academy](https://digital-research.academy/), we mostly provide training for a fee. 
Organisations, projects or PhD programs come to us and we provide the training that they need.
In that sense we don't have grant funding for providing RSE training, but often training can be part of grants.
We do have grants for other topics already (see our OSCARS project [OSPARK](https://oscars-project.eu/projects/ospark-bootcamp-open-science-promotion-and-advocacy-research-knowledge-bootcamp)), and are looking towards being a smaller partner as training provider on (software) grants.

What this example showcases is that you can get funding for training in different ways.
Also, in your next grant - whatever it may be about - please think of us and add training to your grant (we're happy to help) ;).

Do or did you work on a project where software is/was involved? [Let me know](mailto:heidi@heidiseibold.de) and I will happily add it as another case study!
{: .notice--primary}

# Practical Strategies

Here are some thoughts on practical strategies for acquiring research software grant funding.

Acquiring research grant funding that includes provisions for developing research software requires a strategic approach. Here are some practical strategies:

###  Target the Right Funding Opportunities
   - Look for specific grants: Seek grants explicitly aimed at research software development or maintenance. See e.g. my [previous post]({% post_url 2025-01-16-funding-through-rse-1 %}) for examples.
   -  Interdisciplinary programs: Consider interdisciplinary grants that value computational tools, as these often include software development.
   - Look beyond your regular grants. Software funding often comes from foundations (CZI, Sloan, Volkswagen, ...) and big consortia projects may be a good spot to place software that is needed by a wider community.

###  Clearly Articulate the Software's Impact
   -  Address community needs: Demonstrate that the software addresses a gap in tools or capabilities for a particular research community.
   -  Scalability and reuse: Highlight how the software can be used across multiple projects, disciplines, or institutions.
   -  Open science focus: Emphasize alignment with open science and reproducibility initiatives.

###  Impress Reviewers with a Strong Software Development Plan
   -  Methodology: Use software engineering best practices like version control, testing, and documentation.
   -  Sustainability: Show how the software will be maintained post-grant, through community involvement, institutional support, or additional funding.
   -  Metrics for success: Define clear goals for software usability, adoption, or performance.

###  Build Strong Collaborative Teams
   - Multidisciplinary collaboration: Include both domain experts and professional research software engineers (RSEs).
   - Engage user communities: Collaborate with researchers who will use the software and can provide feedback.

###  Justify Software Development as Integral
   - Tie development to research objectives: Show how the software is a critical enabler for achieving the grant's scientific aims.
   - Demonstrate efficiency gains: Argue that the software will reduce costs or time for future research.

###  Show Pilot or Prototype Work
   - Showcase a proof of concept: Having a minimal viable product (MVP) or prototype can make the proposal more compelling.
   - Include preliminary data: Demonstrate initial results achieved using the software.
   
###  Showcase that This is a Good Investment for the Funder
   - Share a plan on what you will do after the funding runs out.
   - Show that you have thought about maintenance, [bus factors](https://en.wikipedia.org/wiki/Bus_factor), and continued community engagement.
   - Think outside the box: Could there be a future business offering support for the (open source) software? Will someone get tenure due to this grant and be able to maintain it for a long time afterwards? Will there be an association for the software project? Will you be able to get private investors in the future?


Any info missing this post? [Let me know](mailto:heidi@heidiseibold.de) and I will happily add it!
{: .notice--primary}

