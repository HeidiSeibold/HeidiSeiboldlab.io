---
layout: single
title:  "How to Get Funding By Caring About Research Software"
date:   2025-01-16
excerpt: "More and more funders are recognizing the vital role of software in research. How can you leverage your software skills to aquire funding?"
header:
  teaser: /assets/images/posts/git.png
toc: true
toc_sticky: true
---

In this post I want to share:

> Recent developments on the status of software in research and funding.

Stay tuned also for an upcoming post on:
> Case studies and practical strategies for securing funding.

Did I miss anything in this post? [Let me know](mailto:heidi@heidiseibold.de) and I will happily add it!
{: .notice--primary}

This will be one of my longest posts ever, but I think it may be helpful for you.


![](/assets/images/posts/git.png)


Let's start with a few examples of how funders recognize software more now and then I'll share a list of funding opportunities.

# Funders start recognizing software
More and more funders are noticing that research software is kind of important
for research. So, for example, I want to list a few noticable developments:

## ADORE
Let's begin with the Amsterdam Declaration on Funding Research Software Sustainability ([ADORE](https://adore.software)).
The [signatories](https://adore.software/signatories/) endorse the ADORE recommendations on improving research software practice, the ecosystem, personnell, and ethics. Among the signatories are the German Research Foundation (DFG), the Research Council of Norway (Forskningsrådet), the Dutch Research Council (NWO), and the Digital Research Alliance of Canada.    
ADORE also provides a [toolkit](https://adore.software/toolkit/), that may be interesting for you to take a look at in terms of securing funding as well as getting your organisation to do the right thing.

## DFG
Since we were already speaking of the German Research Foundation (DFG), let's take a look at the developments here. If you're not interested in Germany, feel free to skip this section.        
The DFG published a handout in 2024 on [*Handling of Research Software in the DFG’s Funding Activities*](https://www.dfg.de/en/basics-topics/basics-and-principles-of-funding/research-software).
This handout gives advice on software/code considerations when applying for funding from DFG.
Also check out the [Information for applicants: the (further) development of research software as part of project funding](https://www.dfg.de/en/basics-topics/basics-and-principles-of-funding/research-software/applicants), if you're applying for funding from DFG. In their [Guidelines for Reviews](https://www.dfg.de/de/formulare-10-20-246348) the DFG asks their reviewers to assess the "achievement of a researcher [...] in its entirety" (including software), so that's also pretty useful to know.

## EU + Horizon Europe
The EU has also noticed that software is important. In their [Open Science policy](https://research-and-innovation.ec.europa.eu/strategy/strategy-research-and-innovation/our-digital-future/open-science_en) practices such as "providing immediate and unrestricted open access to scientific publications, research data, **models**, **algorithms**, **software**, protocols, **notebooks**, **workflows**, and all other research outputs". Many of those relate to code and software (marked in bold).    
The EU funding programme [Horizon Europe](https://research-and-innovation.ec.europa.eu/funding/funding-opportunities/funding-programmes-and-open-calls/horizon-europe_en) evaluates research proposals based on the "quality and appropriateness of the open science practice" (see [Factsheet: Open science in Horizon Europe](https://op.europa.eu/en/web/eu-law-and-publications/publication-detail/-/publication/9570017e-cd82-11eb-ac72-01aa75ed71a1)). Horizon Europe also funded [EVERSE](https://everse.software/about/objectives/), a project focused research software and code excellence with an substantial funding volume (6,789,468.75 Euros for a 36 months project, [see here](https://www.openaire.eu/everse)).

# Funding opportunities

The following opportunities are a collection of things I found myself plus funding listed in the current version of the [ADORE toolkit](https://adore.software/toolkit/).

Funding I found:
* [Themed Week 'Digital Competences in Science'](https://www.volkswagenstiftung.de/de/foerderung/foerderangebot/themenwoche-digitale-kompetenzen-der-wissenschaft-beendet) - Volkswagenstiftung (closed)
* [Scientific Software](https://klaus-tschira-stiftung.de/foerderungen/naturwissenschaftliche-software/) – Klaus Tschira Stiftung (closed)  
* [“Research Software Infrastructures” funding programme](https://www.dfg.de/en/research-funding/funding-opportunities/programmes/infrastructure/lis/funding-opportunities/research-software-infrastructures) - DFG (Submission deadlines in March and August)    
* [Foundational Open Source Software Tools Essential to Biomedicine](https://chanzuckerberg.com/newsroom/czi-awards-16-million-for-foundational-open-source-software-tools-essential-to-biomedicine/) - Chan Zuckerberg Initiative, CZI (I think it's closed, but not sure)
* [Calls from the eScience Center](https://www.esciencecenter.nl/calls-for-proposals/) are generally about code/software - eScience Center Netherlands (currently none open)
* [Infrastructure Steering Committee Grants Program](https://r-consortium.org/all-projects/) - R Consortium (call is held two times a year, in March and September)


Adopted list from the [ADORE toolkit](https://adore.software/toolkit/):
* [Institutional Support for Open Source Software in Research](https://sloan.org/programs/digital-technology/ospo-loi). Focuses on Open Source Program Offices (OSPOs) - Alfred P. Sloan Foundation (closed)
*  [Platforms Program](https://ardc.edu.au/program/platforms-program/) - Australian Research Data Commons, ARDC (closed)
* [Software re-use in research](https://www.canarie.ca/to-spur-software-re-use-in-research-canarie-awards-up-to-3-4m-to-research-teams-to-evolve-their-platforms-for-use-by-other-researchers/) - Canadian Network for the Advancement of Research, Industry and Education, CANARIE (closed)
* [Essential Open Source Software for Science](https://chanzuckerberg.com/eoss/) - Chan Zuckerberg Initiative, CZI (currently closed, but several cycles so new funding may come up this year) 
* [Open & Reusable Research Data & Software](https://www.chistera.eu/call-ord-announcement) - CHIST-ERA (closed)
* [Open Science Fund](https://www.nwo.nl/en/researchprogrammes/open-science/open-science-fund) - Dutch Research Council, NWO (currently closed, but new funding rounds may come)
* [FAIR-IMPACT Route 2 support](https://fair-impact.eu/2nd-open-call-route-2-support) includes support actions focused on assessment and improvement of research software - EOSC FAIR-IMPACT (currently closed, but new funding rounds may come)
* [Call for Proposals to Increase the Usability of Existing Research Software](https://www.dfg.de/foerderung/info_wissenschaft/2022/info_wissenschaft_22_85/index.html) - DFG (closed)  
* [High Priority Open-Source Science](https://nspires.nasaprs.com/external/solicitations/summary.do?solId=%7BB364DBB8-390B-744D-013F-8F4C304B9A63) - NASA (closed)
* [Support for Open-Source Tools, Frameworks, and Libraries (OSTFL)](https://nspires.nasaprs.com/external/solicitations/summary.do?solId=%7b910CC61E-4616-9958-C26F-F8D9BC5AB8D9%7d&path=&method=init) - NASA (closed)  
* [Cyberinfrastructure for Sustained Scientific Innovation (CSS)](https://www.nsf.gov/publications/pub_summ.jsp?ods_key=nsf21617) - US National Science Foundation, NSF (closed)
* [Call for Sustainable Software](https://www.esciencecenter.nl/wp-content/uploads/2023/01/SS-2023-Sustainable-software-Call-2023.pdf) - Netherlands eScience Center (closed)
* [Enhancing biomedical and health-related data and digital platform resources](https://www.ukri.org/opportunity/enhancing-biomedical-and-health-related-data-and-digital-platform-resources/) - UKRI (closed)  

**For even more, see: [Research Software Funding Opportunities](https://www.researchsoft.org/funding-opportunities/) from the Research Software Alliance (ReSA) and the NumFOCUS [fundraising list](https://github.com/numfocus/project-fundraising/issues?q=is%3Aissue%20) (see issues).**

That's all for now. Stay tuned for next week's post with case studies and practical tips for securing funding.

Any info missing this post? [Let me know](mailto:heidi@heidiseibold.de) and I will happily add it!
{: .notice--primary}

Thanks to [Yani](https://fosstodon.org/@yabellini/113843881303979639) for sharing the R Consortium grant info and [Daniel](https://fosstodon.org/@danielskatz/113844426086999115) for the NumFOCUS list.
{: .notice}

