---
layout: single
created: 2024-08-02T10:23:30 (UTC +02:00)
source: https://heidiseibold.ck.page/posts/avoiding-fraud-and-improving-rigor-through-open-science
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/iUauns5JJHeeRUn8QGfRKc
title: Avoiding fraud and improving rigor through Open Science
excerpt: "Fraud in science continues to be a highly discussed topic in the scientific community and also in mainstream media. I've always seen Open Science as a way to improve rigor in science, but can it also avoid fraud?"
---



I will not discuss cases of (alleged) fraud or scientific misconduct in this post, but focus on how I believe we can avoid it in the future. If you are interested in learning more about one of the major recent scandals, I recommend listening to the [Freakonomics radio episode](https://freakonomics.com/podcast/why-is-there-so-much-fraud-in-academia/) on the Gino case (see box below).

<table><tbody><tr><td as="td"><div><figure><p><img src="https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/iUauns5JJHeeRUn8QGfRKc" alt="" width="100%" height="auto"></p></figure></div></td><td></td><td as="td"><div><h2>Why Is There So Much Fraud in Academia?</h2><p>Freakonomics radio episode on the Gino case with wonderful interviews. (I fully disagree with the hosts assessment of the quality of research in economics, but otherwise a great episode)</p>
<a href="https://freakonomics.com/podcast/why-is-there-so-much-fraud-in-academia/">Listen to the podcast</a>
</div></td></tr></tbody></table>

## Focus on the solution

Scientific fraud hurts everyone. It hurts the trust in science, it hurts the scientists who build upon problematic research, and of course it hurts the people who were supposed to benefit from the research (think e.g. medical research).

So how do we avoid fraud? In my opinion, **Open Science can be part of the solution**. If we work on setting better standards in science that promote **openness, collaboration and success based on quality rather than quantity**, we would get a long way already.

As long as we hype single researchers, allow them to work in their ivory tower without sharing relevant details of their research, and promote based on the number of "high impact" publication, we will see scientific misconduct and fraud over and over again.

## Open Science: Avoiding fraud and improving rigor

The cool thing about Open Science is that it goes much beyond just avoiding fraud. **Open Science is all about scientific rigor and good scientific practice**. I, as and Open Science advocate, don't just advocate for openness, I advocate for better quality in research.

Let me share some thoughts on how Open Science avoids fraud <u>and</u> improves rigor at the same time.

<table><tbody><tr><td as="td"><div><h2>Avoid fraud</h2><ul><li><span>Openness and good practices allow you to stay on top of what your are doing.</span></li><li><span>Openness provides the possibility for others to check your work.</span></li><li><span>An open and fair error culture give the possibility to allow saying you were wrong in the past.</span></li><li><span>...</span></li></ul></div></td><td></td><td as="td"><div><h2>Improve rigor</h2><ul><li><span>Open Science promotes good practices such as preregistration and version control.</span></li><li><span>Openness allows you to build on sources you can check and trust.</span></li><li><span>A collaborative culture allows for many people to work together successfully.</span></li><li><span>...</span></li></ul></div></td></tr></tbody></table>

And let me make one thing clear: **Scientific rigor does not mean being right all the time, but doing your best with good intentions.** We are all humans and we make mistakes. My research has been far from perfect, but I did my best and I focused on good research rather than pushing out as many papers as possible.

If you are interested in avoiding fraud and promoting rigor at your institution, feel free to [reach out to me](https://heidiseibold.com/contact/). I am **happy to help with training and support**.


All the best,

Heidi
