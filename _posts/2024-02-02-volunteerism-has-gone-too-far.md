---
layout: single
created: 2024-08-02T10:16:15 (UTC +02:00)
source: https://heidiseibold.ck.page/posts/volunteerism-has-gone-too-far
date: 02.02.2024
read_time: true
show_date: true
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/cnGiHo7FjUAtn9F6xGnCj5
title: Volunteerism has gone too far!
excerpt: "I have volunteered for many initiatives and was always happy to do so. Recently I started to realize that being able to volunteer is a privileged position to be in. To be truly fair and allow all enthusiasts to join an initiative we have to rethink the way we do things. A constructive criticism."
---



I've got to get this off my chest, you all! I've received yet another email asking whether I could speak at an event. For free.

**"Why for free?"** you ask?

> **It's an event from and for enthusiasts.**

I am a fan of a lot of the volunteer-based initiatives in the research community, but **I think we have gone too far.**

-   Too many things are expected to be done for free in the scientific community.
-   Too many things are not budgeted into grants as they could be.
-   Too many groups think about the actual costs of work and time too late or not at all.

Enthusiasm for something does not mean that you can and want to work for it for free.

<table><tbody><tr><td as="td"><div><figure><p><img src="https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/cnGiHo7FjUAtn9F6xGnCj5" alt="Shrugging stick figure" width="200" height="200"></p></figure></div></td><td></td><td as="td"><div><h2>Give everyone a chance to contribute</h2><p>Many people are passionate about something but still do not have the possibility to volunteer. Let's stop building upon volunteer work only, as it excludes people.</p><p>"It will look good on your CV" or "it will give you visibility" is not enough for some of us.</p></div></td></tr></tbody></table>

## How do we fix it?

I'm gonna be honest: the first version of this newsletter was an angry rant. It felt good to write, but it wasn't true to my goal of creating positive impact (in a positive way). So let me try to provide constructive criticism and share some ideas on how to fix the over-reliance on volunteer work.

<table><tbody><tr><td as="td"><div><h3>💸</h3><h3><strong>Ask for (more) money</strong></h3><p>If we provide services (e.g. training, software, project management), we can ask for money - or at least donations - from the institutions and people who benefit.</p><p><em>Ask for money. If you already are and it's not enough, ask for more. Pay the people who contribute a fair share.</em></p></div></td><td></td><td as="td"><div><h3>📊</h3><h3><strong>Plan for the work in grant applications</strong></h3><p>In grants we sometimes forget how much time developing training, organising community events, or mentoring costs.</p><p><em>Put in the real numbers and pay people for the actual work they put in.</em></p></div></td><td></td><td as="td"><div><h3>👩💼</h3><h3><strong>Think like a business, not like a charity</strong></h3><p>Initiatives who provide relevant services for research, are not doing charity. Let's stop acting like it.</p><p><em>Write a business plan, do marketing, sales, and learn from businesses how to make money and pay people.</em></p></div></td></tr></tbody></table>

What do you think? Do these make sense?

## How does the Digital Research Academy do things?

If I know a group of enthusiasts, it's the folks in the [DRA community](https://digital-research.academy/trainers/). We're in this, because we believe that research could be better and we want to make it so. But - and this is important - we don't want to do it for free. Why? Because not all of us can work for free. **Basing what we do on volunteer work would hinder our endeavor of being an open and inclusive community.**

Two of our [guiding principles](https://digital-research.academy/principles/#guiding-principles) are:

-   Together we aim to be an active, inclusive and open community of trainers and learners based on trust, making it a common practice to listen to and support each other and embed inclusive and open practices in our trainings; and
-   We ensure everyone in the network is paid or rewarded for their efforts, we use clear licences for our materials and a clear pricing structure that includes tailored training.

**Have we figured it all out? Of course not!** Some of us have to volunteer for now to get the [Digital Research Academy](https://digital-research.academy/) started until we earn the money to pay ourselves. Also, we are struggling with reactions from our training clients who did not expect our prices (which would be a cheap in industry, especially since our courses are custom).

Some folks want to volunteer their time. That's ok, too. We just don't want to be dependent on it.


I hope that I managed to turn the post this week into something constructive and I am happy to discuss with anyone to find even better solutions. Because I think we can.

All the best,

Heidi

