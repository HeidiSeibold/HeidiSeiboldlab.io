---
layout: single
created: 2024-08-02T10:43:26 (UTC +02:00)
source: https://heidiseibold.ck.page/posts/code-clubs-good-coding-practices-for-all
show_date: true
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/j4C6nfeLQxs6nRoc28gDwX
title: Code Clubs - good coding practices for all
excerpt: "Code clubs are initiatives where people meet to improve their coding skills and help each other out. I am a big fan of them as I believe that they can help improving the quality of research. Because as we all know: Better Software, Better Research."
---

I recently learned more about the code club at the Max Planck Institue of Psychiatry (MPIP) in Munich and would like to share how they organize it in the hopes that it might inspire you. Thanks to the organizers Vera Karlbauer and Jonas Hagenberg for helping me write this post.

## CodeClub at MPIP

**Target audience:** researchers who use code but may have quite varying programming skills (from wet-lab scientists to bioinformaticians).

**Goal:** increase the quality of their research software and code scripts for better reproducibility and fewer errors.

**Structure:**

Meetings: 1x per month, ca. 1h

Format:

*   Short presentations (ca. 10 min) about a relevant topic
*   Short exercises in small groups

Code Review:

*   Platform to find a partner
*   Help with how to perform a code review
*   Discuss problems and experiences in the group

**Implementation:**

*   Ask everyone to present – does not need to be polished
*   Set realistic goals (only 1h)
*   Use breakout rooms for exercises when online
*   Mix experienced with less experienced participants
*   Collect presentations at one place

**Organizers:** Two PhD students (currently Vera and Jonas). They recruit a new candidate from the attending students when one of the two PhD students graduates.

Implementing a code club can be challenging. In their talk, Vera and Jonas mentioned the problems, but they also found some solutions:

*   Beginners are reluctant to join:
    *   Communicate and advertise during onboarding
    *   Low entry barrier and easy first wins (use a [project template](https://github.com/jonas-hag/analysistemplates)!)
*   Wide range of different projects and software.
    *   Topics that are programming language agnostic but useful for many, e.g. literature review tools.
    *   Promote a [project template](https://github.com/jonas-hag/analysistemplates) for better comparability.
    *   Best practice template scripts for R and Python (still work in progress).
*   Often data analysis scripts instead of software development.
    *   Be pragmatic. E.g. instead of full documentation or github code reviews, code comments may be enough.
*   Code review is time-consuming and is not considered an important contribution.
    *   Long-term solution: change culture and recognize achievements.
    *   Clear [code review guide](https://zenodo.org/records/8432946).
    
![Vera presenting the project template that came out of their code club inspired by a template I mentioned to her in a workshop. Made me really happy!](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/nh6c7cBnGiY6UPfQtA5mMT)

## Other code clubs

There are many similar initiatives and quite some useful resources. Here is a list of a bunch of them. Please check them out and consider starting join a code club or even starting one ;)

*   [HackyHour](http://hackyhour.github.io/) projects (see if there is one near you!)
*   [ROpenSci coworking](https://ropensci.org/blog/2023/06/21/coworking/) sessions
*   [TidyTuesday](https://github.com/rfordatascience/tidytuesday) for R users
*   [DataDojo](https://ddojo.github.io/) (Würzburg)
*   [ResBaz](https://resbaz.github.io/resbaz2021/) = Research Bazaar (e.g. the one in [Arizona](https://researchbazaar.arizona.edu/))
*   Mozilla Study Groups (see their [handbook](https://mozillascience.github.io/studyGroupHandbook/)!)



## Support needed?

Need help with starting a code club or implement other strategies in your group/institution/project to improve the quality of your code?
The [Digital Research Academy](https://digital-research.academy/) does just that. 
[Get in touch!](https://digital-research.academy/contact/)

All the best,

Heidi

- - -

P.S. If you're enjoying this newsletter, please consider supporting my work by leaving a tip.
