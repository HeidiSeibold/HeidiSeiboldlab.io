---
layout: single
title:  "Making my newsletter FAIR"
date:   2024-08-02
excerpt: "I am in the process of making my newsletter FAIR (Findable, Accessible, Interoperable, and Reusable). Here's how."
header:
  teaser: /assets/images/posts/2024-08-02_posts-setup.jpg 
---


<!-- 
header:
  teaser: {{ site.url }}/assets/images/posts/2024-08-02_posts-setup.jpg 
  -->

I am an advocate for Open Science. The FAIR priciples are very near and dear to my heart. 

I was excited when the Open Science folks at Jülich archived a bunch of my posts and gave them a persistent identifier ([DOI](https://www.doi.org/the-identifier/what-is-a-doi/), see [archived posts here](https://juser.fz-juelich.de/search?ln=en&cc=FullTexts&sc=1&p=https%3A%2F%2Fheidiseibold.ck.page&f=&action_search=Search)).
This was exactly what I was still missing to make my posts more FAIR. However, I cannot expect them to take care of archiving all my posts and I would love to have all of them available with not only a DOI, but also in a more machine-readable way. That is why now I am finally getting to it and am making all future posts **available, searchable, machine-readable, reusable with an open license (CC-BY 4.0) and with a persistent identifier (DOI)**. 

I spent the whole day on this and here's the plan that I came up with:

### Infrastructure
*This section is for the tech nerds here who want to know all the details.
If you're not one of them, that's ok. You don't need to know what Jekyll is :wink:.* 
{: .notice--primary}

I am planning on using the following infrastructure setup:

- [Jekyll website](https://heidiseibold.com/posts) (hosted via GitLab pages), that allows me to write Markdown posts and creates an Atom feed.
- [Kit newsletter](https://heidiseibold.ck.page/profile) service that can turn an Atom feed into newsletter posts automatically.
- Add my blog to [Rogue Scholar](https://rogue-scholar.org/), an archive for scholarly blogs, which provides long-term archiving, DOIs and metadata.

I really like using Kit for [my newsletter](https://heidiseibold.ck.page/profile) and love creating email courses and
automations with it. To make my newsletter FAIR, however, it's not quite suitable,
because the only way I could archive it and generate a DOI would be manually. Knowing
myself, I'd probably forget doing that.

So, I am switching to using two tools instead of one and I am getting even more
exposure by publishing on Rogue Scholar. Isn't that something? :smiley:

My website is already set up. I am using [Jekyll](https://jekyllrb.com/) ([Minimal Mistakes theme](https://mmistakes.github.io/minimal-mistakes/)) and have a [GitLab repository](https://gitlab.com/HeidiSeibold/HeidiSeibold.gitlab.io) from where my website is automatically built (see [pipelines](https://gitlab.com/HeidiSeibold/HeidiSeibold.gitlab.io/-/pipelines)). Adding a blog is incredibly easy, as Jekyll is meant for writing blogs and with the [`jekyll-feed`](https://github.com/jekyll/jekyll-feed) plug-in an Atom feed is generated without me having to understand how it works (which is good, because I don't). 


### Workflow

From now on I will do the following when I want to write a newsletter post:

1. Write post in Markdown.
2. Push post to GitLab repositor to build my website, which automatically generates the Atom feed.
3. Adapt the automatically generated newsletter and send it to my subscribers.
4. Check if the post shows up on Rogue Scholar (which it automatically should).

![]({{ site.url }}/assets/images/posts/2024-08-02_posts-setup.jpg){: width="250" style="display: block; margin: 0 auto" }

### Does this make my newsletter FAIR?
Well, FAIR is not binary, but a spectrum...

I think having a unique and persistent identifier in the form of a DOI and 
improving long-term availability makes the newsletter **more FAIR**.

I am open for your suggestions on how I could make it even more FAIR in the
future. 

I am also open to hearing what your thoughts are about my new setup, even
if I am a little scared :grimacing:


I am ending the day quite pleased with myself and hope you enjoy reading my
self-centered post of the day :tada:

All the best,   
Heidi