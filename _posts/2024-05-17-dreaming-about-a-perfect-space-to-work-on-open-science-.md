---
layout: single
title:  "Dreaming about a perfect space to work on Open Science"
date:   2024-05-17
excerpt: "Spaces where you can gather with like minded people to discuss, learn, and collaborate can be really powerful. Could a space like this exist for Open Science?"
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/uECfsxtfDcU718gP86z4LH
---


Walking along the little stream back to Dagstuhl castle (during the seminar on [Research Software Engineering: Bridging Knowledge Gaps](https://www.dagstuhl.de/en/seminars/seminar-calendar/seminar-details/24161)), I thought about how inspiring this place is, how perfectly set up for collaborating, learning from each other and also for having just a fabulous time. 

<table><tbody><tr><td><p><img src="https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/4oGr2nFMa1MWTn9s9udpwc" width="250"></p></td><td><p><img src="https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/wrJKGhdpUrtMF2NbycYkVh" width="250"></p></td></tr><tr><td></td></tr></tbody></table>

All the rooms, from the regular meeting rooms to more special ones such as the room of armors are equipped with flip charts; there is a wonderful garden to have conversations in; the guestrooms are simple, functional, and have views into the green surrounding; people are randomly assigned a seat in the dining hall during lunch and dinner and the food is good.

[Dagstuhl](https://www.dagstuhl.de/en) is internationally famous for it's seminars in Computer Science. [Oberwolfach](https://www.mfo.de/) is the equivalent for Mathematics, hidden in a valley in the Black Forest, with a similar setup and even seating rules in the dining hall (see [Oberwolfach](https://en.wikipedia.org/wiki/Oberwolfach_problem) problem, for the maths nerds). These places are so inspiring and obviously useful for science as one can see based on their track record of incredible outputs.

Why don't more places like these exist?

For the [Open Science Retreat](https://openscienceretreat.eu/) we find places that have similar features like Dagstuhl or Oberwolfach and rent them for a week. In a conversation at the [last retreat](https://heidiseibold.ck.page/posts/check-out-what-happened-at-the-open-science-retreat-2024) we started dreaming about a "**permanent Open Science Retreat place**". 

*What if you could rent a room in a building that was bustling with Open Science activities? What if you could organise Open Science related seminars with just the right infrastructure in a place surrounded by nature? What if this place provided a home for small groups who wanted to focus on improving the way we do science for a few weeks?*

![The House of Open Science]({{ site.url }}/assets/images/posts/house-of-open-science.jpeg)

*We could use this place to create systemic change, allow exhausted researchers to reboot, run hackathons, summer schools, seminars, workshops, or celebrate our achievements in improving the quality of research...*

This little daydream Danny and I had at the retreat keeps coming up in my mind. One day I want to make it happen. If you have any ideas on how to, let me know. In the meantime I will look out for possibilities and maybe in 10 years our dream will come true. Who knows. The idea is out there now. That is a start.

All the best,  
Heidi
