---
layout: single
title:  "Incorporation of the Digital Research Academy"
date:   2024-08-23
excerpt: "I am excited to share that the Digital Research Academy incorporation is almost done. We had our notary appointment last week, set up the bank account and are now waiting for the official registration of the DRA Digital Research Academy GmbH."
header:
  teaser: /assets/images/posts/2024-08_incorporation.jpg
---

I published my [first post](https://heidiseibold.ck.page/posts/feedback-wanted-building-a-digital-research-academy) about the initial idea for the Digital Research Academy in May 2023. 
Now, just a year and a few months later, the [Digital Research Academy](https://digital-research.academy/) is becoming not only an initiative but a company.

Last week Joyce Kao, Melanie Imming and I went to the notary and signed all the relevant documents. Now we are just waiting for the entry in the the German company register in order for the **DRA Digital Research Academy GmbH** to be fully official.

![Mel, Joyce, and Heidi at the notary]({{ site.url }}/assets/images/posts/2024-08_incorporation.jpg)

It is incredible to look back at the last year and see what we've achieved:
- We wet up a growing community of >30 amazing trainers and we keep on growing. There is a train-the-trainer program running in Delft right now, onboarding 11 new trainers.
- We formed a core team that can efficiently make decisions and do all the necessary work in the background. I am so glad that Joyce is on board as co-executive director. I could not have done any of this alone and it feels like almost a miracle how well we work together.
- We ran a bunch of trainings together already and the requests keep coming in, even during the summer holidays. We've done trainings on AI in science, research data management, reproducible research and more. And we're about to do some bigger projects on podcasting for science communication and on creating visibility for Open Science. 
- We are not only sucessfully charging fair fees for our trainings in order to pay the trainers fairly, but we've just got notice of the acceptance of two grant proposals, which will help us tremendously in creating financial stability for Joyce's and my salary in the upcoming year. 

Of course there are many things we can and want to do better in the future and lots for us to learn as we move into this next phase. It's scary to start a company. Tax regulations, new processes we need to comply to, the insecurity of whether we'll be able to make a living with this passion project. 
Yet, I feel more at ease that I have felt in a long time. I love being part of a team again instead of the lone freelancer and I truly believe that what we are doing here is worthwhile. We are part of a solution. We help improve the quality of research and maybe even of the research culture.


## Want to collaborate?

We started the Digital Research Academy in order to improve the quality of research and to promote Open Science, data literacy and research software engineering as key building blocks for good research.

We are officially a company, but that does not mean that we suddenly turned evil money monsters. It is the best form for what we want to do (being based in Germany). Trust me, I've done a lot of research and talked to many people, but non-profits in Germany are not sensible for a small entity as the DRA (that may change in the future once we grow). 

Now, we are open for cool projects, would be happy to 
- serve with our trainings and events,
- participate as contractors or partners in grants -> funders often pay training as part as your grant, so think about it! ;), 
- collaborate on cool projects,
- and more.

Please [get in touch!](https://digital-research.academy/contact/)






