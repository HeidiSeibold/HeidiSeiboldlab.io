---
layout: single
title:  "Early career researchers want Open Science - what about the supervisors?"
date:   2024-05-24
excerpt: "Students and early PhD candidates often don't understand all the talk about Open Science. They think Open Science is just 'normal' science. The older researchers are the ones who teach them otherwise. Let's change that!"
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/wj8bJCWiWcZFYgeCmFyY8a
---


In the process of publishing my first PhD paper, the journal asked whether I would like to publish open access. For me that was a no-brainer. When I talked to my supervisor, he said something along the lines of "you're gonna be an expensive PhD student".

I want to mention that my supervisor was always supportive of my Open Science and reproducibility activities and actually taught me a lot in this regard. What is interesting about this story is that I did not think that publishing open access would be expensive in that journal. Why should it be more or less expensive than closed access? Haha 😅, how naive I was... or was I?

Maybe the ones being truly naive, are the ones who don't question a scientific system that is closed, unequal, competitive, yes even hostile sometimes! Why can't science be open, collaborative, creative, and equitable?

## Early career researchers want Open Science
Already in 2017 a paper with the title "[Early career researchers want Open Science](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-017-1351-7)" was published by - you guessed it - a group of early career researchers (ECRs). I see the same in my workshops and courses on Open Science. The ECRs want to learn how to practice Open Science. They see it as the right way to do science. 

[![Screenshot from the paper website https://genomebiology.biomedcentral.com/articles/10.1186/s13059-017-1351-7](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/wj8bJCWiWcZFYgeCmFyY8a)](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-017-1351-7)

**So what is holding them back? It's fear, it's time pressures, it's shame. But it's also their supervisors.**

(Of course not all of them, but I think you get where I am going with this)

## What do supervisors want?
(Again, simplified version here for the sake of the argument)

Supervisors want to publish papers, because that is the currency in academia.
Supervisors want ECRs to publish papers, because they get to be on the paper.
Supervisors know a certain way of how to succeed in academia and they will teach it. Because they also want their PhD candidates and employees to succeed, they will tell them their way.

So, what supervisors do is tell the ECRs to focus on publishing papers in what they consider "good journals."
- Open Access? - Let's publish in this high impact journal instead!
- Open Code? - You'll only make yourself vulnerable, focus on writing a good methods section instead!
- FAIR/Open Data? - Don't worry about that, you'll just loose time! 
- Reproducibility? - Do that on the side. How hard can it be?!
- ...

## Supervisors: rethink!
If we want Open Science to become the norm, if we want high quality and reproducible research that others can build upon, we need supervisors to rethink. 
- Open Access is important to create equity and allow everyone to read the published literature and build upon each others work.
- FAIR/Open Data and Open Code allow others to really understand what you have done and reuse what you've created. This will accelerate research and improve the quality of research.
- Reproducibility (in the definition of same data + same analysis = same result) is really just a minimum standard that everyone should strive for and at the same time it takes time and is hard. Yet if we don't use reproducible research practices (see image below), we usually use a lot of time and nerves in revision and we definitely loose out on quality.

![Reproducible Research: 6 helpful steps. (1) Get your files + folders in order; (2) Use good names for files, folders, functions, …; (3) Document with care: README, Metadata, code comments, …; (4) Version control code, text, …; (5) Stabilize computing environment and software; (6) Publish your research outputs: Code, data, documents, …"]({{ site.url }}/assets/images/posts/steps-reproducible.jpg)

So, supervisors, do you want to continue with your research as you have done in the past and encourage your staff to do the same or do you want to strive for better and equitable research instead?

You play a vital role here. You can make a difference. 

Need help with that? Please do [reach out]({{ site.url }}/contact/) ! To me or other Open Science enthusiasts. We're quite nice 😉

All the best,   
Heidi
