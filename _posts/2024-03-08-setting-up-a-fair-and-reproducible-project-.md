---
layout: single
title:  "Setting up a FAIR and reproducible project "
date:   2024-03-08
excerpt: "Making your research or code project FAIR (Findable, Accessible, Interoperable, Reusable) and reproducible can feel like a chore. But if you have access to the right templates and resources, it can be quite the simple and rewarding task. Let's make it easy for ourselves to do the right thing!"
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/5e34FAQcUkicwLeVrvJLwD
---







So today let me share some templates for setting up #FAIR and #reproducible projects. I asked folks on social media what suggestions they had and got so many that I was not able to put them all here without making everyone's head explode ;). Please do follow the respective social media links if the links here are not enough for you (especially Mastodon). Thanks to everyone who contributed to this list! 🫶

[MASTODON](https://fosstodon.org/@HeidiSeibold/111805033271869754) |
[LINKEDIN](https://www.linkedin.com/posts/dr-heidi-seibold_fair-reproducible-activity-7155522228040654849-KnzI?utm_source=share&utm_medium=member_desktop) |
[TWITTER](https://twitter.com/HeidiBaya/status/1749756437117432008)

Please note that the following research project templates generally assume that you work with data and code. The templates for code projects further down focus on research software projects.

## 🗃️ Research projects
- GIN-Tonic [standardized research folder structure](https://gin-tonic.netlify.app/standard)
- The Turing Way [reproducible project template](https://github.com/the-turing-way/reproducible-project-template)
- My research [project template](https://github.com/HeidiSeibold/research-project-template)
- R [analysistemplates](https://github.com/jonas-hag/analysistemplates) from the MPIP code club
- The [TIER Protocol](https://www.projecttier.org/tier-protocol/protocol-4-0/)
- R user project [template](https://github.com/crsh/template) by Frederik Aust & Marius Barth
- [R project template](https://github.com/startyourlab/r-project-template) by start your lab
- [This template](https://osf.io/59gte/) from Crystal Lewis will help you if you want to reorganize your project for sharing it at the end
- R packages for project templates
    - [WORCS](https://cjvanlissa.github.io/worcs/index.html) - Workflow for Open Reproducible Code in Science for R users
    - [workflowr](https://workflowr.github.io/workflowr/) - organized + reproducible + shareable data science in R
    - [starter](https://www.danieldsjoberg.com/starter/) - a toolkit for starting new projects
    - [rrtools](https://github.com/benmarwick/rrtools) - Tools for Writing Reproducible Research in R
    - [targets](https://docs.ropensci.org/targets/) - Function-oriented Make-like declarative workflows for R 

📦 Code projects
- [Python cookiecutter template](https://github.com/Materials-Data-Science-and-Informatics/fair-python-cookiecutter)
- [R devtools](https://devtools.r-lib.org/)

![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/5e34FAQcUkicwLeVrvJLwD)

## Setting up a FAIR and reproducible project repository
There are a couple of building blocks that you will see in (almost) all templates above:
- A README describing the project
- A coherent folder structure
- Names for files and folders that are (1) human readable, (2) computer readable, and (3) consistent

One more thing I want to mention: Since we are talking about project standards, check out the [YODA principles](https://handbook.datalad.org/en/latest/basics/101-127-yoda.html) which are a set of organizational standards for datasets used for data analysis projects.


Today's post is probably the richest post I've shared so far. Full of super useful templates. I hope that makes up for the past weeks of silence from my end. I was very busy and in the upcoming weeks will share with you what has kept me from writing newsletter posts. I promise: it was worth it!

All the best,   
Heidi
