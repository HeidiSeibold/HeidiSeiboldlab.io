---
layout: single
title:  "Check out what happened at the Open Science Retreat 2024!"
date:   2024-05-10
excerpt: "The Open Science Retreat 2024 was a full success. We co-created so many things and had a wonderful time at the Dutch dunes. Read on to learn what came out of the retreat."
header:
  teaser: https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/2MiF9va4giT5KM3kKSxs5Z
---




The Open Science Retreat follows an unconference model. Except for the plenary sessions, all activities are optional. The edition 2024 was mainly organised by OSC-NL. Here's a few of the things we did:
- Unconference sessions: Collaborate on diverse topics from measuring open science to FAIR code (more below)
- Walks: Whether alone, with a mentor, or in a group. 
- Evening fun: Science karaoke, a bar with amazing barkeepers, silent disco, Open Science games, and more. 
- So much more: Old dutch games, free bikes 🚲, deep conversations, spontaneous collaborations, creative outbursts, ...

[View Open Science Retreat 2024 website](https://openscienceretreat.eu/){: .btn .btn--primary}

![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/2MiF9va4giT5KM3kKSxs5Z)

It is incredible, how inspiring, productive and at the same time enjoyable and uplifting this week was for me (and I think for many of us). 

## Outcomes
Let's look at some of the outcomes we generated! There are so many that I am listing only some of them here. 






Bot that shares learning resources
----------------------------------
![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/vHSGfTkgj2d7DWuoAYPKiz)

This new Mastodon bot highlights resources, syllabi, and courses about Open Science and related subjects.

[@FORRT_bot@botsin.space](https://botsin.space/@FORRT_bot)​


Supporting University Change
============================
![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/wzUQzKKjwTMgfuM2byTqLZ)

Lessons learned from the Open Science community

[9 pages packed with helpful infos](https://zenodo.org/records/10966480)​



Research in 2050
================

![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/GTxW7GiNGpFkTPQFafv1d)

[Speculative fiction](https://www.dannygarside.co.uk/open-notebook#/page/speculative%20sci-fi%2Facademia%20in%202050) on research in 2050 and a Wikipedia draft page for [research cooperatives](https://en.wikipedia.org/wiki/Draft:Research_cooperatives).


Fairly FAIR code in 10 steps
============================
![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/i5Mg7gTo5rQgdTnGBVBWwD)

A simple checklist to make code fairly **FAIR**: **f**indable, **a**ccessible, **i**nteroperable, and **r**eusable.

[Read current version here](https://github.com/PGijsbers/fairly-fair/tree/v1.0.0-alpha)​




Measuring Open Science
======================
![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/sQL9kNUXaiYQNu5jdsB5Fr)

Interactive overview of open science surveys, which may be used e.g. to reuse questionnaire items on different open science practices.

[More info here](https://www.researchequals.com/modules/tn38-4q91)​



Open Science practices in staff assessment
==========================================
![](https://embed.filekitcdn.com/e/7cms4RU2BCZob5MkVBeTcm/zd7Nw1CPiSJrgfGzznYNd)

Guidance document to encourage and take into account Open Science practices during assessment interviews, such as annual reviews.

[Read here](https://zenodo.org/records/10904114)​


Further outputs are still being finalized and will be uploaded to our output collection.

[View R= collection](https://www.researchequals.com/collections/mrhg-9r){: .btn .btn--primary}


Thanks to everyone who came and who supported this year's retreat. It was incredible!

All the best,   
Heidi
