---
permalink: /portfolio/
title: "Workshop Portfolio"
header:
  overlay_image: "../assets/images/Header-Image.png"
author_profile: false

feature_dra:
  - image_path: /assets/images/DRA_LogoColourPos.jpg
    alt: "DRA Logo"
    title: "Digital Research Academy"
    excerpt: "I am involved in creating a trainer and training network, called the Digital Research Academy."
    url: "https://digital-research.academy/"
    btn_class: "btn--primary"
    btn_label: "Learn more!"
---



- [Reproducible Data Science Crash Course](rds_crashcourse)
- [Open Science Crash Course](os_crashcourse)
- [Open Science Hero Workshop](os_hero)
- [Reproducible Data Science Summer School](rds_school)
- ... 

Ask me about further workshop ideas that fit your needs!



All workshops and trainings are available in German and English language. 

[Get in touch!](/impressum/){: .btn .btn--success}

---

{% include feature_row id="feature_dra" type="left" %}
