---
layout: splash
permalink: /testimonials/
feature_row:
  - image_path: /assets/images/testimonials/Lisa-BCCN.jpg
    excerpt: "*It was a really great experience having Heidi at the retreat of our Graduate Program. Our students keep telling me that the Open Science workshop was the highlight of the retreat.*"
    title: "Lisa Velenosi, graduate program coordinator, Bernstein Center for Computational Neuroscience"
  - image_path: /assets/images/testimonials/Angela-HelmholtzAI.jpg
    excerpt: "*Engaging and Energetic - two words that describe Heidi. She did an excellent job as the moderator of our Helmholtz AI conference 2022.  She not only successfully led through our packed agenda but also asked pertinent and insightful questions and was able to engage the audience and to evoke meaningful discussions.*"
    title: "Dr. Angela Jurik-Zeiller, Scientific Manager, Helmholtz AI"
  - image_path: /assets/images/testimonials/ECRs.png
    excerpt: "*Heidi’s workshop on Open Science was a fantastic experience. Since our group consists of young researchers from different health-related disciplines, it was very inspiring for us to learn how manifold the perspectives on Open Science can be. Heidi encouraged us to experience not only how to reflect and bundle our research within our working group, but also what strategies can be helpful to share our perspectives in the science network.*"
    title: "Working group Early Career Researchers, German Society for Social Medicine and Prevention:
Andreas Staudt,
Maieli Fiedler,
Jonathan Uricher,
Dennis Jepsen,
Rosemarie Schwenker"

---

{% include feature_row type="left" %}


