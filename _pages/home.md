---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#6a0498"
  overlay_image: "/assets/images/night.jpg"
excerpt: "Open and Reproducible Data Science"
feature_row:
  - image_path: /assets/images/workshop.jpg
    alt: "."
    title: "Workshops + Training"
    excerpt: "I offer workshops and trainings on Reproducible Data Science and on Open Science."
  - image_path: /assets/images/rope.jpg
    alt: "."
    title: "Consulting"
    excerpt: "Let me help you get the best out of your projects. May it be about data
science, open science or community management."
  - image_path: /assets/images/conference.jpg
    alt: "."
    title: "Events"
    excerpt: "Book me as a speaker in live or online events. I also organize and moderate events. "
#  - image_path: /assets/images/W-media.png
#    alt: "."
#    title: "Media"
#    excerpt: "I produce audio and video material."
#    url: "/podcasts/"
#    btn_class: "btn--primary"
#    btn_label: "Check out my podcasts"

feature_dra:
  - image_path: /assets/images/DRA_LogoColourPos.jpg
    alt: "DRA Logo"
    title: "Digital Research Academy"
    excerpt: "I am involved in creating a trainer and training network, called the Digital Research Academy."
    url: "https://digital-research.academy/"
    btn_class: "btn--primary"
    btn_label: "Learn more!"
---

<!-- ![](/assets/images/Heidi_round.png){: width="250" .align-center} -->


{% include feature_row id="feature_dra" type="left" %}

---

# Services:

{% include feature_row %}



All services are available in German and English language. 
{: .text-center}

[View Testimonials](/testimonials/){: .btn .btn--success .btn--large}
{: .text-center}

---

<p align="center">
<script async data-uid="17875ed937" src="https://heidiseibold.ck.page/17875ed937/index.js"></script>
</p>



