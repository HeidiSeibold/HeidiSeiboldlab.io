---
permalink: /portfolio/os_hero/
---

## Open Science Hero Workshop
---

|**Format:**        | 2-3 days, in person |
|**Size:**          | 5 - 35 participants |
|**Prerequisites:** | None |

---

Through this workshop you will be able to turn your research project into a
fully open and reproducible project. This is an interactive workshop, where you
work on your project, get all the necessary infos, and get all your questions
answered. 


#### Topics:
- Fears and difficulties of working open
- Organizing files and folders
- Reproducible research practices  
- Publication of research output

