---
permalink: /portfolio/rds_crashcourse/
---

## Reproducible data science crash course

---

|**Format:**        | Online, 2.5 hours including breaks |
|**Size:**          | 5 - 35 participants |
|**Prerequisites:** | None |

---

In this workshop you will learn about the most important reproducible research
practices and start implementing them. This includes project organization and
publication of research output (data, code, etc.). At the end of the workshop
you will have a good folder and file organization of your research project and
know how and where to share your work with the world.  This is a practical
workshop where you will improve your current research project.

#### Topics:

- Organizing files and folders
- Good reproducible research practices
  - Working with code (instead of Excel or clicking)
  - An overview on version control
  - What to tackle after the workshop?
- Publication of research output
  - Licensing    
  - Data privacy
  - Platforms
