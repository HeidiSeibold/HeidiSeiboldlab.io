---
permalink: /portfolio/os_crashcourse/
---

## Open Science Crash Course

---

|**Format:**        | Online or in person, 3 hours |
|**Size:**          | 5 - 35 participants |
|**Prerequisites:** | None |

---

In this workshop we will get a kick-start on Open Science practices. This
includes project organization and publication of research output (data, code,
etc.). At the end of the workshop you will have a good folder and file
organization of your research project and know how and where to share your work
with the world.  This is a practical workshop where you will improve your
current research project. 

#### Topics:
- Fears and difficulties of working open
- Organizing files and folders
- Reproducible research practices  
- Publication of research output

