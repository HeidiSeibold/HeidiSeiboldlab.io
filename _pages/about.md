---
title: About
permalink: /about/
aside: true
---

I am a solopreneur in Open and Reproducible Research and an independent
researcher at [IGDORE](https://igdore.org/). I am co-founder of the 
[Digital Research Academy](https://digital-research.academy/) and
[Open Science Freelancers](https://open-science-freelancers.gitlab.io/).

My research has been in the intersection of **Data Science, Reproducibility
and Medicine**. I studied statistics and did my PhD in Biostatistics with a focus
on Machine Learning. 

I strongly believe that Open Science is the way we should do science.

I am happiest when on a bicycle :bike:.

![](../assets/images/bike.jpg)




##### How I became a solopreneur

I was struggling to be an Open Science ambassador and a researcher at the same time. It was impossible for me to follow the rules I was trying to change, so I quit my job as a researcher and made a commitment to dedicate my professional life to improve the way we do research by being an Open Science trainer and consultant. I want to help researchers in doing meaningful, reproducible and trustworthy research. 

---

This website was built with [Jekyll](https://jekyllrb.com/) and the
[Minimal Mistakes](https://mmistakes.github.io) theme.

<script async data-uid="17875ed937" src="https://heidiseibold.ck.page/17875ed937/index.js"></script>

